package com.accenture.twittergradscreenplaycucumber.runner;


import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features="src/test/resources/features/Edit.feature",
        glue= {"com.accenture.twittergradscreenplaycucumber.step_definitions"})


public class EditRunner {
}
