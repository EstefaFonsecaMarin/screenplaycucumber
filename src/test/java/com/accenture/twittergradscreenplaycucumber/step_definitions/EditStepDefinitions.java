package com.accenture.twittergradscreenplaycucumber.step_definitions;

import com.accenture.twittergradscreenplaycucumber.model.LoginModel;
import com.accenture.twittergradscreenplaycucumber.questions.PublicEdit;
import com.accenture.twittergradscreenplaycucumber.task.EditProfile;
import com.accenture.twittergradscreenplaycucumber.task.TwLogin;
import com.accenture.twittergradscreenplaycucumber.ui.TwHomePage;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.EventualConsequence.eventually;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

public class EditStepDefinitions {

    @Managed()private WebDriver driver;
    private Actor ef = Actor.named("Estefa");
    private TwHomePage Twitter;

    @Before
    public void setup() {
        ef.can(BrowseTheWeb.with(driver));
    }

    @Given("^you access the Twitter page and enter the credentials of '(.*)' and '(.*)'$")
    public void youAccessTheTwitterPageAndEnterTheCredentialsOfEstefon12hotmailcomAndQazs2014(String user, String pass) throws Throwable {
        ef.wasAbleTo();
        ef.wasAbleTo(TwLogin.at(Twitter, new LoginModel(user,pass)));
    }

    @When("^you proceed edit my profile in the biography part adding '(.*)'.$")
    public void youProceedEditMyProfileInTheBiographyPartAddingThisIsAnAutomaticTest(String description) throws Throwable {
      ef.wasAbleTo(new EditProfile(description));
    }

    @Then("^you should see the modification in the bigrafia of my profile '(.*)'.$")
    public void youShouldSeeTheModificationInTheBigrafiaOfMyProfileThisIsAnAutomaticTest(String user) throws Throwable {
        ef.should(eventually(seeThat(PublicEdit.ofTweet(), equalTo(user))));


    }
}