package com.accenture.twittergradscreenplaycucumber.step_definitions;

import com.accenture.twittergradscreenplaycucumber.model.LoginModel;
import com.accenture.twittergradscreenplaycucumber.task.TwLogin;
import com.accenture.twittergradscreenplaycucumber.task.TweetTrends;
import com.accenture.twittergradscreenplaycucumber.ui.TwHomePage;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

public class TrendsStepDefinitions {

    @Managed() private WebDriver driver;
    private Actor ef = Actor.named("Estefa");
    private TwHomePage Twitter;


    @Before
    public void setup() {
        ef.can(BrowseTheWeb.with(driver));
    }

    @Given("nter the credentials of '(.*) and '(.*)'$")
    public void thatYouAccessTheTwitter(String user, String pass) throws Throwable {
        ef.wasAbleTo();
        ef.wasAbleTo(TwLogin.at(Twitter, new LoginModel(user,pass)));
    }

    @When("^you proceed to look for the fifth trend in Twitter and get the hashtag '(.*)'$")
    public void you_proceed_to_look_for_the_fifth_trend_in_twitter_and_get_the_hashtag(String prueba) {
        ef.wasAbleTo(new TweetTrends(prueba));

    }

    @Then("^Then a tweet is published with the hashtag '(.*)'$")
    public void then_a_tweet_is_published_with_the_hashtag(String user) {
        //ef.wasAbleTo();
        //ef.should(eventually(seeThat(PublicTrends.ofTweet(), equalTo(user))));

    }

}
