Feature: Modify the user profile in Twiter
  I as a Twitter user
  I want to be able to modify my user profile
  To do a test.

  Background: Access the social network Twitter.
    Given you access the Twitter page and enter the credentials of 'estefon-12@hotmail.com' and 'Qazs.2014'

  @tag
  Scenario: edit profile
    When you proceed edit my profile in the biography part adding 'This is an automatic test'.
    Then you should see the modification in the bigrafia of my profile 'This is an automatic test'.
