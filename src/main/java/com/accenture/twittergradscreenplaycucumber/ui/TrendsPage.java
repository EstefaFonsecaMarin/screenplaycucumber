package com.accenture.twittergradscreenplaycucumber.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;


public class TrendsPage extends PageObject {

    public static final Target Tweet = Target.the("Tweet").locatedBy("//button[@id='global-new-tweet-button']");
    public static final Target Message = Target.the("Message").locatedBy("//div[@class='RichEditor RichEditor--emojiPicker']//div[@name='tweet']");
    public static final Target Send = Target.the("Send").locatedBy("//div[@id='Tweetstorm-tweet-box-0']//div[@class='TweetBoxToolbar-tweetButton']//button[@type='button']");
}
