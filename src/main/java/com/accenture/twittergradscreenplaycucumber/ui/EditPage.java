package com.accenture.twittergradscreenplaycucumber.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class EditPage extends PageObject {

    public static final Target PROFILE = Target.the("Profile").locatedBy("//a[@class='u-textInheritColor js-nav']");
    public static final Target BUTTONEDIT = Target.the("ButtonEdit").locatedBy("//div[contains(@class,'u-textLeft')]//button[@type='button']");
    public static final Target EDITDESCRIPTION = Target.the("EditDescription").locatedBy("//div[@id='user_description']");
    public static final Target BUTTONSAVE = Target.the("ButtonSave").locatedBy("//button[contains(@class,'ProfilePage-saveButton EdgeButton EdgeButton--secondary')]");
    public static final Target DESCRIPTION = Target.the("Description").locatedBy("//p[@class='ProfileHeaderCard-bio u-dir']");
    public static final Target REFRESH=Target.the("Refresh").locatedBy("//li[@id='user-dropdown']");
    public static final Target REFRESHTWO=Target.the("RefreshTwo").locatedBy("//ul[@aria-labelledby=\"user-dropdown-toggle\"]//li[4]");
    public static final Target MESSAGE=Target.the("Message").locatedBy("//div[@class='message']");





}
