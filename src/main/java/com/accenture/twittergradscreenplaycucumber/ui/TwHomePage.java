package com.accenture.twittergradscreenplaycucumber.ui;


import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

import java.util.List;

@DefaultUrl("https://twitter.com/login")

public class TwHomePage extends PageObject {
	//public static  final  Target LOGIN = Target.the("Login").locatedBy("//a[@class='StaticLoggedOutHomePage-input StaticLoggedOutHomePage-narrowLoginButton EdgeButton EdgeButton--secondary EdgeButton--small u-floatRight']");
	public static final Target USERNAME = Target.the("User").locatedBy("//input[@placeholder='Teléfono, correo o usuario']");
	public static final Target PASSWORD = Target.the("Password").locatedBy("//div[@class='clearfix field']//input[@placeholder='Contraseña']");


	public List<WebElementFacade> LIST() {
		List<WebElementFacade> LIST = findAll(By.xpath("//a[@data-query-source=\"trend_click\"]//span"));
		return LIST;
	}
}
