package com.accenture.twittergradscreenplaycucumber.task;

import com.accenture.twittergradlescreenplaycucumber.exceptions.ExceptionsClass;
import com.accenture.twittergradscreenplaycucumber.ui.TrendsPage;
import com.accenture.twittergradscreenplaycucumber.ui.TwHomePage;
import com.accenture.twittergradscreenplaycucumber.ui.TweetProfile;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyEnabled;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class TweetTrends implements Task {

    String Trends;
    TwHomePage twHomePage;

    public TweetTrends (String trends){
        this.Trends = trends;

    }


    @Override
    public <T extends Actor> void performAs(T actor) {
    TrendsTweet(actor);
    }

    private <T extends Actor> void TrendsTweet(T actor){

        String Texto = twHomePage.LIST().get(4).getText();

        try {
            actor.attemptsTo(Click.on(TrendsPage.Tweet),
                    Enter.theValue(Texto + " " + Trends).into(TweetProfile.Text),
                    Click.on(TrendsPage.Send),
                    WaitUntil.the(TweetProfile.Image,isVisible()),
                    Click.on(TweetProfile.Image),
                    WaitUntil.the(TweetProfile.Profile,isCurrentlyEnabled()),
                    Click.on(TweetProfile.Profile)
                    );
        }catch (Exception e) {
            throw new ExceptionsClass(ExceptionsClass.MESSAGE_FAILED_TWEET, e);
        }
    }
}
