package com.accenture.twittergradscreenplaycucumber.task;

import com.accenture.twittergradlescreenplaycucumber.exceptions.ExceptionsClass;
import com.accenture.twittergradscreenplaycucumber.ui.EditPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.*;

public class EditProfile implements Task {

    String Description;

    public EditProfile(String description) {
        // TODO Auto-generated constructor stub
        this.Description = description;
    }



    @Override
    public <T extends Actor> void performAs(T actor)
    {
        edit(actor);
    }

    private <T extends Actor> void edit(T actor) {

        try {
            actor.attemptsTo(Click.on(EditPage.PROFILE),
                   // WaitUntil.the(EditPage.PROFILE,isVisible()),
                   // Click.on(EditPage.PROFILE),
                    WaitUntil.the(EditPage.BUTTONEDIT,isVisible()),
                    Click.on(EditPage.BUTTONEDIT),
                    WaitUntil.the(EditPage.EDITDESCRIPTION,isVisible()),
                    Enter.theValue(Description).into(EditPage.EDITDESCRIPTION),
                    Click.on(EditPage.BUTTONSAVE),
                    WaitUntil.the(EditPage.MESSAGE , isNotCurrentlyVisible())

            );

        }catch (Exception e){
            throw new ExceptionsClass(ExceptionsClass.MESSAGE_FAILED_TRENDS, e);


        }

    }
}
