package com.accenture.twittergradscreenplaycucumber.questions;

import com.accenture.twittergradscreenplaycucumber.ui.EditPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;


public class PublicEdit implements Question<String> {
    EditPage editPage;
    @Override
    public String answeredBy(Actor actor) {
        return String.valueOf(Text.of(editPage.DESCRIPTION).viewedBy(actor).asString());


    }
    public static PublicEdit ofTweet() {
        return new PublicEdit();
    }

}